import {createMessageQueueChannel} from '../utils/mqUtils';
import {Channel, ConsumeMessage} from 'amqplib';

const add = (a: number, b: number) => a + b;

const addAsync = async (a: number, b: number) => {
  return new Promise(resolve => {
    setTimeout(() => {
      const ret = add(a, b);
      resolve(ret);
    }, 1000);
  });
};

const queueRpcCallCalculator = 'rpc-request-queue-calculator';

type ServiceHandler = (payload: any) => Promise<any>;

const msgPreProcessWrapper =
  (channel: Channel, serviceHandler: ServiceHandler) =>
  async (msg: ConsumeMessage | null) => {
    const correlationId = msg?.properties.correlationId;
    const replyTo = msg?.properties.replyTo;
    const payload = JSON.parse(msg?.content.toString() ?? '{}');
    console.log('Receive message');

    const procPayload = await serviceHandler(payload);

    console.log('Finish process message...');

    channel.sendToQueue(replyTo, Buffer.from(JSON.stringify(procPayload)), {
      correlationId: correlationId,
      expiration: 60 * 1000,
    });
  };

const processRequest: ServiceHandler = async payload => {
  const {method, args} = payload;
  if (method === 'add') {
    return addAsync(args.a, args.b);
  } else {
    return {
      error: 'not implemented yet',
    };
  }
};

const main = async () => {
  const channel = await createMessageQueueChannel();
  await channel.assertQueue(queueRpcCallCalculator);
  await channel.consume(
    queueRpcCallCalculator,
    msgPreProcessWrapper(channel, processRequest),
    {
      noAck: true,
    }
  );
};

main();
