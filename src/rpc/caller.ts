import {Channel, ConsumeMessage} from 'amqplib';
import {createMessageQueueChannel} from '../utils/mqUtils';
import {randomUUID} from 'crypto';

const queueRpcDone = 'rpc-response-queue';
const queueRpcCall = 'rpc-request-queue';

const makeRpcRequest = async (
  channel: Channel,
  service: string,
  method: string,
  args: object,
  ttlMs = 5000
) => {
  const correlationId = randomUUID().toString();

  const doRequestPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error(`The request time exceeds TTL ${ttlMs}ms`));
    }, ttlMs);

    channel.consume(
      queueRpcDone,
      response => {
        if (response?.properties.correlationId !== correlationId) {
          return;
        }

        const data = JSON.parse(response?.content.toString() ?? '{}');

        if (data?.error) {
          reject(data.error);
        }

        resolve(data);
        channel.cancel(response.fields.consumerTag);
      },
      {
        noAck: true,
      }
    );
  });

  const strMessage = JSON.stringify({
    method,
    args,
  });

  channel.sendToQueue(`${queueRpcCall}-${service}`, Buffer.from(strMessage), {
    replyTo: queueRpcDone,
    correlationId: correlationId,
  });

  return doRequestPromise;
};

const main = async () => {
  const channel = await createMessageQueueChannel();
  await channel.assertQueue(queueRpcDone);

  for (let i = 0; i < 10; ++i) {
    const response = await makeRpcRequest(channel, 'calculator', 'add', {
      a: 3,
      b: 5,
    });
    console.log(`${i}th iter: ${response}`);
  }
};

main();
