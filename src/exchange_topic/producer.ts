import {createExchange, createMessageQueueChannel} from '../utils/mqUtils';
import {topicMessages} from '../message/topicMessages';
import {getRoutingKeyFromTopic} from '../utils/messageProcessor';

const EXCHANGE_NAME = 'test_exchange_topic';
const initializeRabbitMQ = async () => {
  const channel = await createMessageQueueChannel();
  await createExchange(channel, EXCHANGE_NAME, 'topic');
  return channel;
};

initializeRabbitMQ()
  .then(channel => {
    topicMessages.forEach(message => {
      const routingKey = getRoutingKeyFromTopic(message.topic);
      const strMessage = JSON.stringify({...message.body, routingKey});

      channel.publish(EXCHANGE_NAME, routingKey, Buffer.from(strMessage), {
        persistent: true,
      });
      console.log(
        `[Producer] Publish message ${message.body.content} to ${EXCHANGE_NAME} with routing key ${routingKey}`
      );
    });

    return channel;
  })
  .then(channel => channel.close())
  .catch(error => console.log(error));
