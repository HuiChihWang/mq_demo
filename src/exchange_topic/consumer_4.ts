import {consumeMessage} from '../utils/messageProcessor';
import {
  bindQueueToExchange,
  createExchange,
  createMessageQueueChannel,
  createQueue,
} from '../utils/mqUtils';

const QUEUE_NAME = 'task_queue_topic_4';
const EXCHANGE_NAME = 'test_exchange_topic';
const initializeRabbitMQ = async () => {
  const channel = await createMessageQueueChannel();
  await createQueue(channel, QUEUE_NAME);
  await createExchange(channel, EXCHANGE_NAME, 'topic');
  await bindQueueToExchange(
    channel,
    QUEUE_NAME,
    EXCHANGE_NAME,
    'normal.email.*'
  );
  return channel;
};

initializeRabbitMQ()
  .then(async channel => {
    await channel.consume(
      QUEUE_NAME,
      async message => consumeMessage(channel, message),
      {
        noAck: false,
      }
    );
  })
  .catch(error => console.log(error));
