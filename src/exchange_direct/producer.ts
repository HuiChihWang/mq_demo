import {createExchange, createMessageQueueChannel} from '../utils/mqUtils';
import {fakeMessages} from '../message/basicMessages';

const EXCHANGE_NAME = 'test_exchange';
const initializeRabbitMQ = async () => {
  const channel = await createMessageQueueChannel();
  await createExchange(channel, EXCHANGE_NAME);
  return channel;
};

initializeRabbitMQ()
  .then(channel => {
    fakeMessages.forEach(message => {
      const strMessage = JSON.stringify(message);
      channel.publish(
        EXCHANGE_NAME,
        message.routingKey,
        Buffer.from(strMessage),
        {
          persistent: true,
        }
      );
      console.log(
        `[Producer] Publish message ${message.content} to ${EXCHANGE_NAME}`
      );
    });

    return channel;
  })
  .then(channel => channel.close())
  .catch(error => console.log(error));
