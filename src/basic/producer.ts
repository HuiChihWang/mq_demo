import {createMessageQueueChannel, createQueue} from '../utils/mqUtils';
import {fakeMessages} from '../message/basicMessages';

const QUEUE_NAME = 'task_queue';
const initializeRabbitMQ = async () => {
  const channel = await createMessageQueueChannel();
  await createQueue(channel, QUEUE_NAME);
  return channel;
};

initializeRabbitMQ()
  .then(channel => {
    fakeMessages.forEach(message => {
      const strMessage = JSON.stringify(message);
      channel.sendToQueue(QUEUE_NAME, Buffer.from(strMessage), {
        persistent: true,
      });
      console.log(`[Producer] Enqueue message ${message.content}`);
    });

    return channel;
  })
  .then(channel => channel.close())
  .catch(error => console.log(error));
