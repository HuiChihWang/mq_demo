import {consumeMessage} from '../utils/messageProcessor';
import {createMessageQueueChannel, createQueue} from '../utils/mqUtils';

const QUEUE_NAME = 'task_queue';

const initializeRabbitMQ = async () => {
  const channel = await createMessageQueueChannel();
  await createQueue(channel, QUEUE_NAME);
  return channel;
};

const main = async () => {
  const channel = await initializeRabbitMQ();
  await channel.consume(
    QUEUE_NAME,
    message => consumeMessage(channel, message),
    {
      noAck: false,
    }
  );
  await channel.close();
};

main();
