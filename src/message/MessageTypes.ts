export interface MQMessage {
  content: string;
  timeInSeconds: number;
  routingKey?: string;
}

export enum Priority {
  URGENT = 'urgent',
  NORMAL = 'normal',
  LAZY = 'lazy',
}

export enum Job {
  EMAIL = 'email',
  PAYMENT = 'payment',
  SUBSCRIPTION = 'subscription',
}

export enum Authority {
  INTERNAL = 'internal',
  USER = 'user',
}

interface MessageBody {
  content: string;
  timeInSeconds: number;
}

export interface Topic {
  priority: Priority;
  jobType: Job;
  auth: Authority;
}

export interface TopicMessage {
  body: MessageBody;
  topic: Topic;
}
