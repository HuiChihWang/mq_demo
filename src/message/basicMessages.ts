import {MQMessage} from './MessageTypes';

export const fakeMessages: MQMessage[] = [
  {
    content: 'one',
    timeInSeconds: 3,
    routingKey: 'info',
  },
  {
    content: 'two',
    timeInSeconds: 1,
    routingKey: 'warning',
  },
  {
    content: 'three',
    timeInSeconds: 5,
    routingKey: 'info',
  },
  {
    content: 'four',
    timeInSeconds: 2,
    routingKey: 'error',
  },
  {
    content: 'five',
    timeInSeconds: 8,
    routingKey: 'error',
  },
];
