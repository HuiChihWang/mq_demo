import {Authority, Job, Priority, TopicMessage} from './MessageTypes';

export const topicMessages: TopicMessage[] = [
  {
    body: {
      content: 'one',
      timeInSeconds: 3,
    },
    topic: {
      priority: Priority.NORMAL,
      jobType: Job.EMAIL,
      auth: Authority.USER,
    },
  },
  {
    body: {
      content: 'two',
      timeInSeconds: 1,
    },
    topic: {
      priority: Priority.URGENT,
      jobType: Job.PAYMENT,
      auth: Authority.USER,
    },
  },
  {
    body: {
      content: 'three',
      timeInSeconds: 5,
    },
    topic: {
      priority: Priority.LAZY,
      jobType: Job.EMAIL,
      auth: Authority.INTERNAL,
    },
  },
  {
    body: {
      content: 'four',
      timeInSeconds: 2,
    },

    topic: {
      priority: Priority.NORMAL,
      jobType: Job.SUBSCRIPTION,
      auth: Authority.USER,
    },
  },
  {
    body: {
      content: 'five',
      timeInSeconds: 2,
    },
    topic: {
      priority: Priority.URGENT,
      jobType: Job.EMAIL,
      auth: Authority.INTERNAL,
    },
  },
  {
    body: {
      content: 'six',
      timeInSeconds: 8,
    },
    topic: {
      priority: Priority.URGENT,
      jobType: Job.SUBSCRIPTION,
      auth: Authority.INTERNAL,
    },
  },
];
