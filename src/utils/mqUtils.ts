import ampq, {Channel} from 'amqplib';
import config from 'config';

const RABBITMQ_HOST = config.get<string>('rabbitmq.host');
const RABBITMQ_PORT = config.get<number>('rabbitmq.port');

export const createMessageQueueChannel = async () => {
  try {
    const connection = await connectToMq(RABBITMQ_HOST, RABBITMQ_PORT);
    const channel = await connection.createChannel();
    console.log('[RabbitMQ]: Connect to message queue successfully.');
    return channel;
  } catch (error) {
    console.log('[RabbitMQ]: Fail to connect to queues.');
    throw error;
  }
};

export const createQueue = async (channel: Channel, queueName: string) => {
  await channel.assertQueue(queueName, {
    durable: true,
  });
};

export const createExchange = async (
  channel: Channel,
  exchangeName: string,
  exchangeType: 'direct' | 'topic' | 'headers' | 'fanout' | 'match' = 'direct'
) => {
  await channel.assertExchange(exchangeName, exchangeType, {
    durable: true,
  });
};

export const bindQueueToExchange = async (
  channel: Channel,
  queueName: string,
  exchangeName: string,
  routingKey: string
) => {
  await channel.bindQueue(queueName, exchangeName, routingKey);
};

export const closeMqConnection = async (channel: Channel) => {
  await channel.close();
  console.log('[RabbitMQ]: Close connection to message queue successfully.');
};

export const connectToMq = async (host: string, port: number) => {
  return ampq.connect({
    hostname: host,
    port: port,
  });
};
