import {MQMessage, Topic} from '../message/MessageTypes';
import {Channel, ConsumeMessage} from 'amqplib';

const processMessage = ({timeInSeconds, content}: MQMessage) =>
  new Promise<void>(resolve => {
    console.log(
      `[Consumer] Process Message: Job '${content}' require ${timeInSeconds} seconds`
    );
    setTimeout(() => {
      resolve();
    }, timeInSeconds * 1000);
  });

export const consumeMessage = async (
  channel: Channel,
  message: ConsumeMessage | null
) => {
  if (!message) {
    throw Error('[Consumer]: Message is null');
  }

  const strMessage = message.content.toString();
  const messageObject = JSON.parse(strMessage) as MQMessage;

  if (messageObject.timeInSeconds > 7) {
    channel.reject(message, false);
    console.log(
      `[Consumer]: Reject message (${messageObject.timeInSeconds} seconds).`
    );
    return;
  }

  await processMessage(messageObject);
  channel.ack(message);
  console.log(
    `[Consumer]: Process message ${messageObject.content} complete with topic ${messageObject.routingKey}.`
  );
};

export const getRoutingKeyFromTopic = (topic: Topic) => {
  const keys = Object.values(topic);
  return keys.join('.');
};
